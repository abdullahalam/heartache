# Thoughts #
A Simple, plain text, note taking app

### Why? ###

Because most note-taking apps focus on features and formatting, and end up bloated.

### Features ###

* Plain text files
* Minimal UI
* Ad-Free
* Open Source
* Password protected hidden section

### But where are the formatting options? ###

There are no formatting options. That is intentional. 

### How to install? ###

From the [Play Store](https://play.google.com/store/apps/details?id=com.antimony.heartache).