package com.antimony.heartache;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

/**
 * Created by antimony on 18/4/17.
 */

public class PasswordActivity extends AppCompatActivity {


    EditText passfield;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
//        this.setFinishOnTouchOutside(false);


        passfield = (EditText)findViewById(R.id.password_field);
    }


    public void passwordEntered(View v){
        String password = passfield.getText().toString();
        Intent passIntent = new Intent();

        passIntent.putExtra("PASSWORD", password);
        setResult(1, passIntent);
        finish();

    }

}
