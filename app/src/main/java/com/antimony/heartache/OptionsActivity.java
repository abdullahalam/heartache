package com.antimony.heartache;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Switch;

/**
 * Created by antimony on 19/4/17.
 */

public class OptionsActivity extends AppCompatActivity {

    Switch save;
    boolean savestate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ativity_options);
        save = (Switch) findViewById(R.id.save_switch);
        savestate = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("AUTOSAVE", false);
        save.setChecked(savestate);

    }

    public void startUpdatePassActivity(View v){
        startActivity(new Intent(this, ChangePassActivity.class));
    }


    @Override
    protected void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("AUTOSAVE", save.isChecked()).commit();
    }
}
