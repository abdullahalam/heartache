package com.antimony.heartache;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by antimony on 19/4/17.
 */

public class ChangePassActivity extends AppCompatActivity {


    String existingpass;
    TextInputLayout oldl, newl, cl;
    EditText olde, newe, ce;
    Button button, rembutton;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        findViewById(R.id.pass_bg).requestFocus();

        existingpass = PreferenceManager.getDefaultSharedPreferences(this).getString("PASSWORD", "null");

        oldl = (TextInputLayout)findViewById(R.id.oldpasslayout);
        newl= (TextInputLayout)findViewById(R.id.newpasslayout);
        cl = (TextInputLayout)findViewById(R.id.confirmpasslayout);

        olde = (EditText) findViewById(R.id.old_pass_field);
        newe = (EditText) findViewById(R.id.new_pass_field);
        ce = (EditText) findViewById(R.id.confirm_pass_field);

        button = (Button) findViewById(R.id.update_pass_button);
        rembutton = (Button)findViewById(R.id.delete_pass_button);

        if(existingpass.equals("null")){
            oldl.setVisibility(View.GONE);
            newl.setVisibility(View.VISIBLE);

            newe.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    cl.setVisibility(View.VISIBLE);
                    ce.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if(ce.getText().toString().equals(newe.getText().toString())){
                                button.setVisibility(View.VISIBLE);
                            }else {
                                button.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


        }else {
            oldl.setVisibility(View.VISIBLE);
            olde.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {


                    if(olde.getText().toString().equals(existingpass)) {

                        newl.setVisibility(View.VISIBLE);
                        rembutton.setVisibility(View.VISIBLE);
                        newe.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                cl.setVisibility(View.VISIBLE);




                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });

                        ce.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {

                                if (ce.getText().toString().equals(newe.getText().toString())) {
                                    button.setVisibility(View.VISIBLE);
                                } else {
                                    button.setVisibility(View.GONE);
                                }

                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        });

                    }else {
                        newl.setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

    }

    public void updatePassword(View v){

        String newpassword = ce.getText().toString();
        if(newpassword.length()>4) {
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString("PASSWORD", newpassword).commit();
            Toast.makeText(this, "New Password Updated!", Toast.LENGTH_SHORT).show();
            finish();
        }else {
            Toast.makeText(this, "Your password needs to be longer than 5 characters!", Toast.LENGTH_SHORT).show();
        }
    }


    public void removePassword(View v){
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("PASSWORD", "null").commit();
        Toast.makeText(this, "Password removed. Anybody can access your hidden notes.", Toast.LENGTH_LONG).show();
        finish();
    }


}
