package com.antimony.heartache;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;


public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    File[] file_list;
    ListView mainlist;
    ArrayAdapter fadapter;
    String path, password;
    int requestcode = 1;
    TextView title;
    boolean showingHidden;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showingHidden=false;

    }

    public void loadContent(boolean hidden){
        try {

            if(!hidden) {
                path = Environment.getExternalStorageDirectory().toString() + "/Outlet";
            }else {
                path = Environment.getExternalStorageDirectory().toString() + "/Outlet/.hidden";
            }

            File location = new File(path);
            if (!location.exists()) {
                location.mkdir();
            }

            file_list = location.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.charAt(0)!='.';
                }
            });


            Arrays.sort(file_list, new Comparator() {
                public int compare(Object o1, Object o2) {

                    if (((File) o1).lastModified() > ((File) o2).lastModified()) {
                        return -1;
                    } else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
                        return +1;
                    } else {
                        return 0;
                    }
                }

            });


            final ArrayList<String> files = new ArrayList<String>();
            files.clear();

            for (File temp : file_list) {
                files.add(temp.getName());
            }

            Collections.sort(files, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.compareToIgnoreCase(o2);
                }
            });

            if(!hidden) {
                fadapter = new ArrayAdapter<String>(this, R.layout.my_list_item, files);
            }else{
                fadapter = new ArrayAdapter<String>(this, R.layout.my_hidden_list_item, files);
            }
            mainlist = (ListView) findViewById(R.id.content_list);
            mainlist.setDividerHeight(0);






            mainlist.setAdapter(fadapter);
            mainlist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                    final String item = (String) parent.getItemAtPosition(position);

                    AlertDialog dialog = new AlertDialog.Builder(MainActivity.this).create();

                    dialog.setTitle("Delete Note?");
                    dialog.setMessage("This action cannot be undone!");
                    dialog.setButton(dialog.BUTTON_POSITIVE, "DELETE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            File to_dl;
                            if(!showingHidden) {
                                to_dl = new File(Environment.getExternalStorageDirectory().toString() + "/Outlet/" + item);
                            }else {
                                to_dl = new File(Environment.getExternalStorageDirectory().toString() + "/Outlet/.hidden/" + item);
                            }
                            boolean del_hua = to_dl.delete();

                            if (del_hua) {
                                Toast.makeText(getApplicationContext(), item + " Deleted!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), item + " Could not be Deleted!", Toast.LENGTH_SHORT).show();
                            }

                            files.remove(position);
                            fadapter.notifyDataSetChanged();

                        }
                    });
                    dialog.setButton(dialog.BUTTON_NEGATIVE, "NOPE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    int del_color = ContextCompat.getColor(MainActivity.this, R.color.colorDelete);
                    int can_color = ContextCompat.getColor(MainActivity.this, R.color.colorCancel);
                    dialog.show();
                    dialog.getButton(dialog.BUTTON_POSITIVE).setTextColor(del_color);
                    dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(can_color);


                    return true;
                }
            });
            mainlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
                    String value = (String) adapter.getItemAtPosition(position);
                    Intent i = new Intent(MainActivity.this, ContentActivity.class);
                    i.putExtra("LISTITEM", value);
                    i.putExtra("MODE", "EXISTING");
                    i.putExtra("HIDDEN", showingHidden);
                    startActivity(i);
                }

            });

            showingHidden=hidden;
        }catch (Exception e){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestcode);
        }
    }

    public void updateTitleColor(){
        if(showingHidden){
            title.setTextColor(Color.parseColor("#E57373"));
        }else {
            title.setTextColor(Color.parseColor("#f5f5f5"));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadContent(showingHidden);
        password = PreferenceManager.getDefaultSharedPreferences(this).getString("PASSWORD", "null");

        title = (TextView)findViewById(R.id.main_title);

        updateTitleColor();

        title.setLongClickable(true);
        title.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                if(!showingHidden){
                    if(!password.equals("null")) {
                        Intent i = new Intent(getApplicationContext(), PasswordActivity.class);
                        startActivityForResult(i, 1);
                    }else {
                        loadContent(true);
                    }
                }else {
                    loadContent(false);

                }

                updateTitleColor();

                return true;
            }
        });

    }

    public void newFile(View v){
        Intent i = new Intent(MainActivity.this, ContentActivity.class);
        i.putExtra("LISTITEM", "null");
        i.putExtra("MODE", "NEW");
        i.putExtra("HIDDEN", showingHidden);
        startActivity(i);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data!=null){
            String message = data.getStringExtra("PASSWORD");
            if(requestCode==1){
                if(message.equals(password)){
                    loadContent(true);
                }else {
                    Toast.makeText(getApplicationContext(), "Incorrect Password", Toast.LENGTH_SHORT).show();
                }
            }
        }else {
            loadContent(false);
        }


    }

    public void showOptions(View v){
        Intent i = new Intent(this, OptionsActivity.class);
        startActivity(i);
    }

}
