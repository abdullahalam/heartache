package com.antimony.heartache;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by antimony on 12/3/17.
 */

public class ContentActivity extends AppCompatActivity{

    Toolbar toolbar;
    File f;
    String path, category;
    EditText editext, title;
    boolean textchanged,is_hidden_file, shouldAutoSave;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_view);

        shouldAutoSave = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("AUTOSAVE", false);

        Intent i = getIntent();
        String titletext = i.getStringExtra("LISTITEM");
        is_hidden_file = i.getBooleanExtra("HIDDEN", false);

        title = (EditText)findViewById(R.id.content_title);
        editext = (EditText)findViewById(R.id.content_field);
        textchanged = false;


        if(!titletext.equals("null")) {

            title.setText(titletext);
            updateEditText(titletext);

        }

        if(is_hidden_file){
            title.setTextColor(Color.parseColor("#E57373"));
        }

        editext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textchanged = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textchanged = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if(!is_hidden_file) {
            f = new File(Environment.getExternalStorageDirectory().toString() + "/Outlet/" + titletext);
        }else {
            f = new File(Environment.getExternalStorageDirectory().toString() + "/Outlet/.hidden/" + titletext);
        }

    }

    public void updateEditText(String filename){

        if(!is_hidden_file) {
            path = Environment.getExternalStorageDirectory().toString() + "/Outlet/" + filename;
        }else {
            path = Environment.getExternalStorageDirectory().toString() + "/Outlet/.hidden/" + filename;
        }

        String cont = "";

        try {
            FileInputStream ipstream = new FileInputStream(new File(path));
            if (ipstream != null){
                InputStreamReader inputStreamReader = new InputStreamReader(ipstream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String gotString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ( (gotString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(gotString).append("\n");
                }

                ipstream.close();
                cont=stringBuilder.toString();
            }


        }catch (FileNotFoundException e){
            //Toast.makeText(getApplicationContext(),"FNF", Toast.LENGTH_SHORT).show();

        }catch (IOException e){
            //Toast.makeText(getApplicationContext(),"IO", Toast.LENGTH_SHORT).show();
        }



        if(cont.length()>1) {
            editext.setText(cont);
        }else {
//            editext.setText("Content");
        }


    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    public void saveContent(){
        if(!is_hidden_file) {
            path = Environment.getExternalStorageDirectory().toString() + "/Outlet/" + title.getText().toString();
        }else {
            path = Environment.getExternalStorageDirectory().toString() + "/Outlet/.hidden/" + title.getText().toString();
        }

        Intent i = getIntent();

        String mode = i.getStringExtra("MODE");

        try {
            File thisfile = new File(path);
            if(mode.equals("EXISTING")) {
                f.renameTo(thisfile);
            }else {
                thisfile.createNewFile();
            }

            FileOutputStream opstream = new FileOutputStream(thisfile);
            OutputStreamWriter owrite = new OutputStreamWriter(opstream);

            owrite.write(editext.getText().toString());
            owrite.close();

        }catch (FileNotFoundException e){
            //Toast.makeText(getApplicationContext(),"FNF", Toast.LENGTH_SHORT).show();
        }catch (IOException e){
            Toast.makeText(getApplicationContext(),"IO", Toast.LENGTH_SHORT).show();
        }
    }

    public void contentMenu(View v){

        PopupMenu mMenu = new PopupMenu(this, v);
        MenuInflater inflater = mMenu.getMenuInflater();
        inflater.inflate(R.menu.contentmenu, mMenu.getMenu());
        mMenu.show();

    }

    @Override
    public void onBackPressed() {

        if(textchanged) {

            if(shouldAutoSave){
                saveContent();
                finish();
            }else {

                AlertDialog dialog = new AlertDialog.Builder(ContentActivity.this).create();
                dialog.setTitle("Do You Wish To Save This File?");
                dialog.setMessage("Any unsaved changes will be lost!");

                dialog.setButton(dialog.BUTTON_POSITIVE, "SAVE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveContent();
                        finish();
                    }
                });

                dialog.setButton(dialog.BUTTON_NEGATIVE, "NOPE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                int del_color = ContextCompat.getColor(ContentActivity.this, R.color.colorDelete);
                int can_color = ContextCompat.getColor(ContentActivity.this, R.color.colorCancel);
                dialog.show();
                dialog.getButton(dialog.BUTTON_POSITIVE).setTextColor(del_color);
                dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(can_color);

            }

        }else {
            finish();
        }

        //super.onBackPressed();

    }

    public void saveButtonClicked(MenuItem m){
       saveContent();
       Toast.makeText(this,"Thoughts Saved", Toast.LENGTH_SHORT).show();
   }
}
